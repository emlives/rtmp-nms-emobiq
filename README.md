## Node Media Server RTMP / NMS eMOBIQ V1

### How to start NMS on eMOBIQ Container 
Pull From Docker HUB and Run the Container: 

1. Start MongoDB
    1. *Run MongoDB with custom path data:* mongod --dbpath /var/lib/mongodb
2. Run the Node Server / NMS RTMP V1
    1. *Go to Path:*  /opt/rtmp-server/server
    2. *Run and save output to log:* node server.js >> /home/log/nms-old.log
    3. *Read the output log while the server running*  

Mongo Credential
mongo -u admin -p Emlive2022#! 127.0.0.1/admin

